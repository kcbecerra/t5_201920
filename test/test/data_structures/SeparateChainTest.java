package test.data_structures;

import junit.framework.TestCase;
import model.data_structures.SeparateChainHashTable;

public class SeparateChainTest extends TestCase{

	SeparateChainHashTable<String,Integer> tabla;

	public void setUp1() throws Exception {
		tabla = new SeparateChainHashTable<String, Integer>(2);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);
		tabla.put("Bistro", 111);
		tabla.put("Ana", 222);
		tabla.put("Bom", 13);
	}

	public void setUp2() throws Exception {
		tabla = new SeparateChainHashTable<String, Integer>(5);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);
		tabla.put("Bistro", 111);

	}

	public void setUp3() throws Exception {
		tabla = new SeparateChainHashTable<String, Integer>(5);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);

	}

	public void testGet1() {
		try {
			setUp1();
			int i = tabla.get("Alberto");
			int j = tabla.get("Bencio");
			int k = tabla.get("Alejandro");
			int l = tabla.get("Bistro");
			int m = tabla.get("Ana");
			int n = tabla.get("Bom");

			assertEquals(i,8);
			assertEquals(j, 12);
			assertEquals(k, 1);
			assertEquals(l, 111);
			assertEquals(m, 222);
			assertEquals(n, 13);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}

	}
	public void testGet2() {
		try {
			setUp3();
			int i = tabla.get("Alberto");
			int j = tabla.get("Bencio");
			int k = tabla.get("Alejandro");

			assertEquals(i,8);
			assertEquals(j, 12);
			assertEquals(k, 1);
		}
		catch(Exception e) {
			fail("Lanza excepcion");
		}
	}
	public void testPut1() {

		try {
			setUp1();
			tabla.put("Jorge", 234);
			tabla.put("Alberto", 213);

			int i = tabla.get("Jorge");
			int j = tabla.get("Alberto");

			assertEquals(i,234);
			assertEquals(j, 213);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}
	}

	public void testRehash() {

		try {
			setUp1();
			tabla.put("Alonso", 8);
			tabla.put("Betty", 12);
			tabla.put("Carolina", 1);
			tabla.put("Diego", 111);
			tabla.put("Edna", 222);
			tabla.put("Fernando", 13);
			int i = tabla.size();
			assertEquals(i,4);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}
	}

	public void testRehash2() {

		try {
			setUp3();
			int i = tabla.size();
			assertEquals(i,5);
		}
		catch(Exception e) {
			fail("Lanza excepcion");
		}
	}

}
