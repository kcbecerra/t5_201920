package test.data_structures;

import junit.framework.TestCase;
import model.data_structures.LinearProbingHashTable;

public class LinearProbingTest extends TestCase{

	LinearProbingHashTable<String,Integer> tabla;

	public void setUp1() throws Exception {
		tabla = new LinearProbingHashTable<String, Integer>(5);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);
		tabla.put("Bistro", 111);
		tabla.put("Ana", 222);
		tabla.put("Bom", 13);
	}

	public void setUp2() throws Exception {
		tabla = new LinearProbingHashTable<String, Integer>(5);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);
		tabla.put("Bistro", 111);

	}

	public void setUp3() throws Exception {
		//No debe hacer rehash
		tabla = new LinearProbingHashTable<String, Integer>(5);
		tabla.put("Alberto", 8);
		tabla.put("Bencio", 12);
		tabla.put("Alejandro", 1);

	}

	public void testGet1() {
		try {
			setUp1();
			int i = tabla.get("Alberto");
			int j = tabla.get("Bencio");
			int k = tabla.get("Alejandro");
			int l = tabla.get("Bistro");
			int m = tabla.get("Ana");
			int n = tabla.get("Bom");

			assertEquals(i,8);
			assertEquals(j, 12);
			assertEquals(k, 1);
			assertEquals(l, 111);
			assertEquals(m, 222);
			assertEquals(n, 13);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}

	}
	public void testGet2() {
		try {
			setUp3();
			int i = tabla.get("Alberto");
			int j = tabla.get("Bencio");
			int k = tabla.get("Alejandro");

			assertEquals(i,8);
			assertEquals(j, 12);
			assertEquals(k, 1);
		}
		catch(Exception e) {
			fail("Lanza excepcion");
		}
	}
	public void testPut1() {

		try {
			setUp1();
			
			tabla.put("Jorge", 234);
			tabla.put("Alberto", 213);

			int i = tabla.get("Jorge");
			int j = tabla.get("Alberto");
			int h = tabla.getNumber();

			assertEquals(i,234);
			assertEquals(j, 213);
			assertEquals(h,8);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}
	}

	public void testRehash() {

		try {
			setUp1();
			int i = tabla.getSize();
			assertEquals(i,10);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			fail("Lanza excepcion");
		}
	}

	public void testRehash2() {

		try {
			setUp3();
			int i = tabla.getSize();
			assertEquals(i,5);
		}
		catch(Exception e) {
			fail("Lanza excepcion");
		}
	}

}
