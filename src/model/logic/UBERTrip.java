package model.logic;

public class UBERTrip implements Comparable<UBERTrip>

{

	//--------------------------------------
	// Atributos
	//--------------------------------------
	/*
	 *  Identificador �nico de la zona de origen.
	 */
	private int sourceid;

	/*
	 *  Identificador �nico de la zona de destino.
	 */
	private int dstid;

	/*
	 * N�mero del d�a (Domingo = 1, etc.)
	 */
	private int dow;

	/*
	 * Tiempo promedio en segundos.
	 */
	private double tiempo;

	/*
	 * Desviaci�n est�ndar
	 */
	private double desviacion;

	/*
	 * Tiempo geometrico.
	 */
	private double tiempoGeometrico;

	/*
	 * Desviaci�n geometrica. 
	 */
	private double desviacionGeometrica;


	//----------------------------------------
	//Constructor
	//----------------------------------------
	/*
	 * 
	 */
	public UBERTrip (int pSourceid, int pDstid, int pDia, double pTiempo, double pDesviacion, double pTiempoG, double pDesviacionG )
	{

		sourceid = pSourceid;
		dstid = pDstid;
		dow = pDia;
		tiempo = pTiempo;
		desviacion= pDesviacion;
		tiempoGeometrico = pTiempoG;
		desviacionGeometrica = pDesviacionG;
	}



	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------

	/**
	 * Retorna el identificador �nico de la zona de oriegn. 
	 * @return Identificador.
	 */
	public int darIdentificador( )
	{
		return sourceid;
	}

	/**
	 * Retorna el identificador del destino.
	 * @return Identificado destino.
	 */
	public int darIdentificadorD( )
	{
		return dstid;
	}


	/*
	 * Retorna el dia de la semana. 
	 * @return dow.
	 */
	public int darDiaDeLaSemana() 
	{
		return dow;
	}


	/*
	 * 
	 */
	public double darTiempo() 

	{
		return tiempo;
	}


	/*
	 * 
	 */
	public double darDesviacion() 
	{
		return desviacion;
	}


	/*
	 * 
	 */
	public double darTiempoGeometrico() 
	{
		return tiempoGeometrico;
	}

	/*
	 * 
	 */
	public double getDesviacionGeometrica() 

	{
		return desviacionGeometrica;
	}

	@Override
	public String toString()
	{
		return (sourceid +", "+  dstid +", " +  "dow");
	}



	@Override
	public int compareTo(UBERTrip that) {
		if(this.dow < that.dow) {
			return -1;
		}
		if(this.dow > that.dow) {
			return 1;
		}
		else {
			return 0;
		}

	}
	
	
}



