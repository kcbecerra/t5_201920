package model.logic;

import java.io.FileReader;


import com.opencsv.CSVReader;

import model.data_structures.DynamicArray;
import model.data_structures.LinearProbingHashTable;
import model.data_structures.SeparateChainHashTable;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	private CSVReader lector;
	private DynamicArray<UBERTrip> lista;
	private int trimestre;
	public LinearProbingHashTable<String,UBERTrip> ubertripLinear;
	public SeparateChainHashTable<String, UBERTrip> ubertripSeparateChain;



	// -------------------------------------------------------------
	// Constantes
	// -------------------------------------------------------------



	// -------------------------------------------------------------
	// Consturctor
	// -------------------------------------------------------------

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		lector = null;
		lista = new DynamicArray<UBERTrip>();
		trimestre=0;
		
	}

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------

	/**
	 * Lectura del archivo. 
	 */
	public void leerArchivo(String pFile, int pTrimestre)
	{

		try
		{
			lector = new  CSVReader(new FileReader (pFile));
			String [] lectura = lector.readNext();

			while ((lectura = lector.readNext())!= null)
			{
				int sourceid1 = Integer.parseInt(lectura[0]);
				int destino1 = Integer.parseInt(lectura[1]);
				int hora = Integer.parseInt(lectura[2]);
				double tiempo1 = Double.parseDouble(lectura[3]);
				double desviacion1 = Double.parseDouble(lectura[4]);
				double tGeom = Double.parseDouble(lectura[5]);
				double dGeom = Double.parseDouble(lectura[6]);

				UBERTrip creado = new UBERTrip(sourceid1,destino1,hora,tiempo1, desviacion1, tGeom, dGeom);
				lista.append(creado);

			}
			System.out.println(lista.size());
			lector.close();

			double tiempo = System.currentTimeMillis();
			for (UBERTrip viaje : lista) {
				ubertripLinear.put(("" + trimestre) + ("-"+ viaje.darIdentificador() ) + ("-"+ viaje.darIdentificadorD()), viaje);
			}
			double tiempo2 = System.currentTimeMillis();
			for (UBERTrip viaje : lista) {
				ubertripSeparateChain.put(("" + trimestre) + ("-"+ viaje.darIdentificador() ) + ("-"+ viaje.darIdentificadorD()), viaje);
			}
			double tiempo3 = System.currentTimeMillis();
			double tiempoTotalLinear = tiempo2 - tiempo;
			double tiempoTotalSeparate = tiempo3 - tiempo2;
			System.out.println("El tiempo linear es:" + tiempoTotalLinear);
			System.out.println("El tiempo separate es:" + tiempoTotalSeparate);
		}

		catch (Exception e)
		{
			e.getMessage();
		}

	}

	public DynamicArray<UBERTrip> darArreglo()
	{
		return lista;
	}




}
