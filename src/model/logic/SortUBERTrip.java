package model.logic;

public class SortUBERTrip {

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param <T>
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */

	public static void ordenarShellSort( Comparable[] datos ) {

		//Tomamos nuestro tama�o del arreglo
		int tamanio = datos.length;

		//Definimos nuestro desplazamiento como un valor premeditado por ahora
		int despl = 1;

		//Con este while vamos a definir el desplazamiento principal, el primerito y m�s grande
		//Un tercio nos ayuda a que sea un poco m�s sim�trico
		while (despl < tamanio/3)
		{
			despl = 3*despl + 1;
		}

		//Aqu� ya definimimos nuestro desplazamiento principal
		//Mientras el desplazamiento sea v�lido
		while (despl >= 1)
		{ 
			//Re loco pero aqu� definimos i como desplazamiento, i nos permite revisar los dem�s datos del arreglo desde despl en adelante
			for (int i = despl; i < tamanio; i++)
			{
				//Este for por pasos:
				//Lo primero es que nos pone j como i (en el primer caso es despl, en otros casos nos permite que j sea otros valores que est�n m�s adelante) y nos pide verificar
				//que sea mayor o igual a despl (si fuera menor nos dar�a indexoutofbounds por ello verifica).
				//Luego, toma nuestro dato j y pregunta si este es menor a j-despl (en la primera entrada compara el primerito con el j porque
				//la resta nos dar� 0).

				for (int j = i; j >= despl && less(datos[j], datos[j-despl]); j -= despl)
				{
					//SI ENTRA: Hace el cambio de posici�n entre ambos y designa j menos el desplazamiento, en donde j literalmente quedar� en j-despl (es decir el que se cambio)
					// Al entrar otra vez, nos permite hacer que ese valor menor se vaya corriendo lo m�ximo posible
					exchange(datos, j, j-despl);
				}
				//SI NO ENTRA:Sea por el less o que nuestro j es menor al desp, no entra a este for, lo pasa, e itera arriba con i++
			}

			//Cuando ya pase todo el arreglo con su primer desplazamiento, lo reduce de tal manera que el desplazamiento sea menor, haya mayor cantidad de comparaciones
			//y sea m�s exacto en cuanto a ordenar
			despl = despl/3;

			//Como est� en un while vuelve a hacer todo pero con un despl m�s peque�o.
		}

	}		



	//Este m�todo es el que nos compara nuestros datos.
	private static void merge(Comparable[] datos, Comparable[] aux, int primerIndex, int mitad, int ultimoIndex)
	{

		//Aqu� copiamos todos los datos en nuestro auxiliar
		for (int k = primerIndex; k <= ultimoIndex; k++)
		{
			aux[k] = datos[k];
		}


		int i = primerIndex;

		int j = mitad+1;

		//Con este for vamos a verificar los datos que nos mandaron, de una vez vamos a poner los datos en nuestro arreglo original de forma ordenada
		for (int k = primerIndex; k <= ultimoIndex; k++)
		{
			//Si es el caso de que el primero es mayor a la mitad
			if (i > mitad) 
			{
				//ponemos en la primer posici�n al siguiente elemento de la mitad.
				datos[k] = aux[j++];
			}

			//Si mitad+1 es mayor a �ltimo index ponemos en primera posici�n al segundo.
			else if (j > ultimoIndex) 
			{
				datos[k] = aux[i++];
			}

			//Si el elemento en mitad+1 es menor al primero
			else if (less(aux[j], aux[i])) 
			{
				//Pone el siguiente a mitad+1 de primeras.
				datos[k] = aux[j++];
			}
			else 
			{
				datos[k] = aux[i++];
			}
		}
	} 


	/**
	 * M�todo que nos parte nuestra muestra seg�n los datos dados.
	 * @param datos. Arreglo de datos a ordenar
	 * @param aux. Arreglo auxiliar para pasar los datos 
	 * @param primerIndex. Posici�n inicial
	 * @param ultimoIndex. Posici�n final del arreglo
	 */
	private static void mergeSort(Comparable[] datos, Comparable[] aux, int primerIndex, int ultimoIndex)
	{
		//Esto con el fin de detener la recursi�n, ya que llega a lo m�s at�mico, es decir un solo elemento.
		if (ultimoIndex <= primerIndex)
		{
			return;
		}

		//SI FALLA SUMAR EL PRIMER INDEX 

		//Aqu� lo que hacemos es encontrar la mitad de la muestra.
		int mitad = primerIndex + (ultimoIndex - primerIndex) / 2;


		//Aqu� hacemos recursi�n y mandamos la primera mitad, en la recursi�n nos comienza a partir hasta lo m�s at�mico.
		mergeSort(datos, aux, primerIndex, mitad);

		//Aqu� hacemos recursi�n y mandamos la segunda mitad, nos comienza a partir la segunda mitad hasta llegar a lo m�s peque�o.
		mergeSort(datos, aux, mitad+1, ultimoIndex);

		//Aqu� nos llega a la �ltima parte, es decir, todo lo que queremos comparar.
		merge(datos, aux, primerIndex, mitad, ultimoIndex);
	} 

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {

		//Este es el m�todo que nos inicia el ordenamiento, lo �nico que hace es recibir la muestra de datos a ordenar,
		//Crea un arreglo auxiliar con el length de nuestro original
		//Manda todo a otro m�todo que nos ayuda a partir.
		Comparable[] aux = new Comparable[datos.length];
		mergeSort(datos, aux, 0, datos.length - 1);

	}


	private static int particion(Comparable[] datos, int primerIndex, int ultimoIndex)
	{
		int i = primerIndex; 
		int j = ultimoIndex+1;
		
		while (true)
		{
			while (less(datos[++i], datos[primerIndex]))
			{
				if (i == ultimoIndex)
				{
					break;
				}
			}

			while (less(datos[primerIndex], datos[--j]))
			{
				if (j == primerIndex)
				{
					break;
				}
			}

			if (i >= j)
			{
				break;
			}
			
			exchange(datos, i, j);
		}
		
		exchange(datos, primerIndex, j);
		return j;
	} 

	private static void quickSort(Comparable[] datos, int primerIndex, int ultimoIndex)
	{
		if (ultimoIndex <= primerIndex)
		{
			return;
		}

		int j = particion(datos, primerIndex, ultimoIndex);

		quickSort(datos, primerIndex, j-1);

		quickSort(datos, j+1, ultimoIndex);
	} 

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {

		shuffle(datos);
		quickSort(datos, 0, datos.length - 1); 
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{

		return v.compareTo(w)==-1? true: false;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable rev=datos[i];
		datos[i]=datos[j];
		datos[j]=rev;

	}


	public static void shuffle(Comparable[] dat)
	{
		for(int i=0; i<dat.length; i++)
		{
			int randominer=(int) (dat.length*Math.random());
			Comparable uno= dat[i];
			Comparable cambio= dat[randominer];
			dat[i]=cambio;
			dat[randominer]=uno;
		}
	}
}

