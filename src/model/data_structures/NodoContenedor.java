package model.data_structures;

public class NodoContenedor<T> {

	
	
private T elemento;
private NodoContenedor<T> siguiente;	
	

	public NodoContenedor(T element)
	{
		siguiente=null;
		elemento=element;
	}
	
	public NodoContenedor<T> getNext()
	{
		return siguiente;
	}
	
	public boolean hasNext()
	{
		return siguiente!=null;
	}
	
	
	public void setNext(NodoContenedor<T> change)
	{
		siguiente=change;
	}
	
	
	public T getElemento()
	{
		return elemento;
	}
	
	public void setElemento(T elem){
		elemento = elem;
	}
}
