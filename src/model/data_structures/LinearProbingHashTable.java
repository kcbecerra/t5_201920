package model.data_structures;


public class LinearProbingHashTable<K extends Comparable <K>,V>
{
	private int number, size; 
	
	private K[] keys;   
	private V[] values;    

	/** Constructor **/
	public LinearProbingHashTable(int capacity) 
	{
		number = 0;
		size = capacity;
		values = (V[]) new Object[capacity];
		keys = (K[]) new Comparable[capacity];
	}  


	public void makeEmpty()
	{
		number = 0;
		keys = (K[]) new Comparable[size];
		values = (V[]) new Object[size];
	}

	public int getNumber() 
	{
		return number;
	}

	public int getSize() 
	{
		return size;
	}
	
	public boolean isFull() 
	{
		return number == size;
	}

	public boolean isEmpty() 
	{
		return getNumber() == 0;
	}

	public boolean contains(K key) 
	{
		return get(key) !=  null;
	}

	protected int hash(K key) {
		return (key.hashCode() & 0x7fffffff) % size;
	} 
	
	public void resize(int m) {
		LinearProbingHashTable<K,V> nuevo = new LinearProbingHashTable<K,V>(m);
		int sizeO = size;
		size = m;
		for (int i = 0; i < sizeO; i++) {
			if (keys[i] != null) {
				nuevo.put(keys[i], values[i]);
			}
		}
		keys = nuevo.keys;
		values = nuevo.values;
		System.out.println("Resize de Probing");
	}

	public void put(K key, V val) 
	{           
		Double fc = (double) ((number+1)/size);
		if(fc>0.75) {
			resize(2*size);
		}
		int tmp = hash(key);
		int i = tmp;
		do
		{
			if (keys[i] == null)
			{
				keys[i] = key;
				values[i] = val;
				number++;
				return;
			}
			if (keys[i].equals(key)) 
			{ 
				values[i] = val; 
				return; 
			}            
			i = (i + 1) % size;            
		} while (i != tmp);       
	}

	public void putInSet(K key, V val) 
	{           
		Double fc = (double) ((number+1)/size);
		int tmp = hash(key);
		int i = tmp;
		do
		{
			if (keys[i] == null)
			{
				keys[i] = key;
				values[i] = val;
				number++;
				return;
			}
			if (keys[i].equals(key)) 
			{ 
				values[i] = val; 
				return; 
			}            
			i = (i + 1) % size;            
		} while (i != tmp);       
		if(fc>0.75) {
			resize(2*size);
		}
	}
	public V get(K key) 
	{
		int i = hash(key);
		while (keys[i] != null)
		{
			if (keys[i].equals(key))
				return values[i];
			i = (i + 1) % size;
		}            
		return null;
	}

	public void delete(K key) 
	{
		if (!contains(key)) 
			return;

		int i = hash(key);
		while (!key.equals(keys[i])) 
			i = (i + 1) % size;        
		keys[i] = (K) (values[i] = null);

		for (i = (i + 1) % size; keys[i] != null; i = (i + 1) % size)
		{
			K tmp1 = keys[i];
			V tmp2 = values[i];
			keys[i] = (K) (values[i] = null);
			number--;  
			put(tmp1, tmp2);            
		}
		number--;        
	}       

	public void printHashTable()
	{
		System.out.println("\nHash Table: ");
		for (int i = 0; i < size; i++)
			if (keys[i] != null)
				System.out.println(keys[i] +" "+ values[i]);
		System.out.println();
	}   
}


