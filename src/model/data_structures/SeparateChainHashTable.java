package model.data_structures;

import java.util.Iterator;


public class SeparateChainHashTable<K extends Comparable <K>,V>
{ 
	private Node<K, V>[] nodes; 

	private int number; 

	private int size; 


	public SeparateChainHashTable(int m) 
	{ 
		size=m;
		number=0;
		nodes= new Node[size];
	} 


	public int size() 
	{ 
		return size; 
	} 


	public boolean isEmpty() { 
		return number == 0; 
	} 

	protected int hash(K key) {
		return (key.hashCode() & 0x7fffffff) % size;
	} 

	public int number(){
		return number;
	}

	public void put(K key, V value)
	{

		if(key==null)
		{
			throw new IllegalArgumentException();
		}

		Double fc = (double) ((number+1)/size);

		int i= hash(key);

		for(Node z= nodes[i]; z!=null; z=z.siguiente())
		{
			if(key.equals(z.darKey()))
			{
				z.cambiarValue(value);
				return;
			}
		}
		number++;
		if(fc>5.00) {
			resize(2*size);
		}
		Node<K,V> nuevo = new Node(key, value, nodes[i]);
		nuevo.setNext(nodes[i]);
		nodes[i] = nuevo;

	}

	public void putInSet(K key, V value)
	{
		Double fc = (double) ((number+1)/size);

		int i= hash(key);

		for(Node z= nodes[i]; z!=null; z=z.siguiente())
		{
			if(key.equals(z.darKey()))
			{
				z.setnextV(value);
				return;
			}
		}
		number++;
		if(fc>5.00) {
			resize(2*size);
		}
		Node<K,V> nuevo = new Node(key, value, nodes[i]);
		nuevo.setNext(nodes[i]);
		nodes[i] = nuevo;
	}


	public V get(K key)
	{
		if(key==null)
		{
			throw new IllegalArgumentException();
		}
		int rev= hash(key);
		Node<K,V> m = nodes[rev];
		while(m!=null){
			System.out.println(m);
			if(key.equals(m.darKey()))
			{
				return (V) m.darValue();
			}
			m=m.siguiente();
		}

		return null;
	}


	public V delete (K key)
	{
		if(key==null)
		{
			throw new IllegalArgumentException();
		}
		int rev= hash(key);
		Node anterior=null;
		for (Node m=nodes[rev]; m!=null; m=m.siguiente())
		{
			if(key.equals(m.darKey()))
			{
				anterior.setNext(m.siguiente());
				number--;
				return (V) m.darValue();
			}
			else
			{
				anterior=m;
			}
		}
		return null;
	}


	public Iterator<K> keys()
	{
		Queue<K> cola= new Queue<K>();

		for(int i=0; i<size; i++)
		{
			for(Node s= nodes[i]; s!=null; s=s.siguiente())
			{
				cola.enqueue((K) s);
			}
		}
		return (Iterator<K>) cola.iterator();
	}



	private void resize(int chains) {
		System.out.println("Resize de Separate");
		SeparateChainHashTable<K,V> nuevo = new SeparateChainHashTable<K,V>(chains);
		int sizeO = size;
		size = chains;
		for (int i = 0; i < sizeO; i++) {
			if (nodes[i] != null) {
				if(nodes[i].darKey()!=null){
					nuevo.put(nodes[i].darKey(), nodes[i].darValue());
				}
			}
		}
		nodes = nuevo.nodes;
	}

}