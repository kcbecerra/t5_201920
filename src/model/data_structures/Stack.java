package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<T> implements IStack<T> {

	private NodoContenedor<T> topStack;
	private int size;
	
	public Stack(){
		topStack = null;
		size = 0;
	}
	public Iterator<T> iterator() {
		return new ElIterator<T>(topStack);
	}

	public boolean isEmpty() {
		if(topStack == null)
		return true;
		else
		return false;
	}

	public int size() {
		return size;
	}
	
	public boolean contains(T elem) {
		if(topStack == null) {
			return false;
		}
		else {
			ElIterator<T> it = (ElIterator<T>) this.iterator();
			while(it.hasNext()) {
				T v = it.next();
				if(v.equals(elem)) {
					return true;
				}
			}
			return false;
		}
	}
	
	public void push(T t) {

		NodoContenedor<T> nuevo =  new NodoContenedor<T>(t);
		if(topStack == null){
			topStack = nuevo;
		}
		else{
			nuevo.setNext(topStack);
			topStack = nuevo;
		}
		size++;
	}

	public T pop() {

		if(topStack == null){
			throw new EmptyStackException();
		}
		else{
		T elem = topStack.getElemento();
		NodoContenedor<T> siguiente = topStack.getNext();
		topStack = siguiente;
		size--;
		return elem;
		}
			
	}
	
}
