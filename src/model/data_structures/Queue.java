package model.data_structures;

import java.util.Iterator;

public class Queue<T> 
{
	private int size;

	private NodoContenedor<T> queueHead;

	private NodoContenedor<T> queueTail;



	public Queue()
	{
		queueHead=null;
		queueTail=null;
	}


	public NodoContenedor<T> tail()
	{
		return queueTail;
	}

	public Iterator<T> iterator() {

		return new ElIterator<>(queueHead);
	}


	public boolean isEmpty() {

		return queueHead==null? true: false;
	}

	public int size() {

		return size;
	}

	public T dequeue() {

		T ret=queueHead.getElemento();
		queueHead=queueHead.getNext();
		size--;

		return ret;

	}


	public void enqueue(T t) {
		
		NodoContenedor<T> enq=new NodoContenedor<T>(t);

		if(isEmpty())
		{
			queueHead=enq;
			queueTail=enq;
			size++;
		}
		else
		{
			NodoContenedor<T> rev=queueTail;
			rev.setNext(enq);
			queueTail=enq;
			size++;
		}		
	}



}
