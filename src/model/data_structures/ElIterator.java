package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ElIterator<T> implements Iterator<T> {

	private NodoContenedor<T> actual;

	public ElIterator(NodoContenedor<T> nuevo)
	{
		actual=nuevo;
	}


	@Override
	public boolean hasNext() 
	{
		if(actual.getNext()!=null)
		{
			return true;
		}
		return false;
	}

	@Override
	public T next() throws NoSuchElementException {

		if(actual==null)
		{
			throw new NoSuchElementException("No hay siguiente");
		}

		T elemento=actual.getElemento();
		actual=actual.getNext();
		return elemento;
	}
	
	
}
