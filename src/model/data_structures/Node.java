package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Node<K extends Comparable<K>, V> {

	private class Value
	{
		private V value;

		private Value siguiente;
		
		public Value (V pValue, Value pSiguiente)
		{
			value = pValue;
			siguiente = pSiguiente;

			
		}
		
		public void setnextV (V valuee)
		{
			value = valuee;
		}
		
	}
	
	
	private K key;

	private Value value;

	private Node<K,V> siguiente;


	public Node(K k, V v, Node<K,V> s)
	{
		key=k;
		value = new Value(v, null);
		siguiente=s;
	}

	public Node(K k, V v)
	{
		key=k;
		value = new Value(v, null);
	}

	public V darValue()
	{
		return value.value;
	}


	public K darKey()
	{
		return key;
	}


	public void cambiarValue(V nuevo)
	{
		value = new Value(nuevo, null);
	}


	public Node<K,V> siguiente()
	{
		return siguiente;
	}

	public boolean hasNext() {

		if(siguiente!=null) {
			return true;
		}
		return false;
	}

	public void setNext(Node <K,V> nodo) {
		siguiente = nodo;
	}

	public void setnextV (V valuee)
	{
		value = new Value(valuee, value);
	}
	
}
