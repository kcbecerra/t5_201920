package view;


public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
				System.out.println("---------ISIS 1206 - Estructuras de datos----------");
				System.out.println("---------------------Taller 5----------------------");
				System.out.println("0. Cargar datos de viajes UBER y tablas");
				System.out.println("1. Retornar las infracciones con accidente a partir de un ADDRESSID con LinealProbingHash");
				System.out.println("2. Retornar las infracciones con accidente a partir de un ADDRESSID con SeparateChainHash");
				System.out.println("3. Salir");

		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		

}
