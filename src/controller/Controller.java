package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;


import model.data_structures.DynamicArray;
import model.data_structures.LinearProbingHashTable;
import model.data_structures.Node;
import model.data_structures.SeparateChainHashTable;
import model.data_structures.Stack;
import model.logic.MVCModelo;
import model.logic.UBERTrip;
import view.MVCView;



public class Controller {

	public static final String file1 = "./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";

	public static final String file2 = "./data/bogota-cadastral-2018-2-WeeklyAggregate.csv";

	public static final String file3 = "./data/bogota-cadastral-2018-3-WeeklyAggregate.csv";

	public static final String file4 = "./data/bogota-cadastral-2018-4-WeeklyAggregate.csv";


	private MVCView view;
	private MVCModelo modelo;
	private DynamicArray<UBERTrip> arreglo;
	public LinearProbingHashTable<String,Stack<UBERTrip>> ubertripLinear;
	public SeparateChainHashTable<String,Stack<UBERTrip>> ubertripSeparateChain;



	public Controller() 
	{
		view = new MVCView();
		modelo = new MVCModelo();
		ubertripLinear = new LinearProbingHashTable<String, Stack<UBERTrip>>(1976);
		ubertripSeparateChain=new SeparateChainHashTable<String, Stack<UBERTrip>>(100);
	}



	public void run()
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 0:

				view.printMessage("Ingrese el trimestre que desea (1-4): ");
				int trimestre = lector.nextInt();
				if(trimestre == 1) {
					modelo.leerArchivo(file1, 1);

				}
				else if(trimestre == 2) {
					modelo.leerArchivo(file2, 2);


				}
				else if(trimestre == 3) {
					modelo.leerArchivo(file3, 3);
				}

				else if(trimestre == 4) {
					modelo.leerArchivo(file4, 4);
				}


				break;



			}
		}
	}


}



